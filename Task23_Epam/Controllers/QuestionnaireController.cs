﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task23_Epam.Controllers
{
    public class QuestionnaireController : Controller
    {
        // GET: Questionnaire
        [HttpGet]
        public ActionResult Questionnaire()
        {
            var ages = new List<string>() { "<18", "18-24", "24-34", "35-50", ">50" };
            var advertising = new List<string>()
            {
                "Advertising in the elevator",
                "Advertising in the subway",
                "TV commercial",
                "Advertising on the street",
                "I read on social networks on the Internet",
                "Recommendations of acquaintances",
                "Passed by"
            };
            var services = new List<string>() { "Video portal", "IPTV", "Internet radio", "Game portal", "Soft" };

            ViewBag.Ages = ages;
            ViewBag.Advert = advertising;
            ViewBag.Services = services;

            return View();
        }

        [HttpPost]
        public string Questionnaire(string gender, string years, string name, string surname, List<string> service, List<string> ser)
        {
            string result = $"<h2>Result</h2>Your gender: {gender} <br />Your age: {years}<br />Your name: {name} <br />Your surname: {surname}<br />";
            result += "You find out about service from: ";
            for (int i = 0; i < service?.Count; i++)
            {
                result += service[i] + ", ";
            }
            result += "<br />Services that interest you: ";

            for (int i = 0; i < ser?.Count; i++)
            {
                result += ser[i] + ", ";
            }
            return result;
        }
    }
}