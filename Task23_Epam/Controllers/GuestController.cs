﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task23_Epam.Controllers
{
    public class GuestController : Controller
    {
        List<(string name, string data, string comment)> comments = new List<(string name, string data, string comment)>
            {
                ("Chris Fox", "25.12.2017", "Good site. 8/10 "),
                ("Rebecca Flex", "10.10.2020", "I want to work in Epam"),
                ("Vasya Pupkin", "12.12.2020", "Year! Me too!"),
                ("Vova D", "13.02.2021", "I personally know the developer ")
            };

        // GET: Guest
        [HttpGet]
        public ActionResult Guest()
        {
            ViewBag.Comments = comments;
            return View();
        }

        [HttpPost]
        public ActionResult Guest(string name, string comment)
        {
            comments.Add((name, DateTime.Now.ToString("d"), comment));
            ViewBag.Comments = comments;
            return View();
        }
    }
}